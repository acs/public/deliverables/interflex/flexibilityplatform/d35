# General

This repository contains a set of example implementations based on the InterFlex Flexibility Platform using the [InterFlex API](https://git.rwth-aachen.de/acs/public/deliverables/interflex/flexibilityplatform/api).
The set of scripts provides interested parties with an insight into the usage of the API while it can also be used for verifying the correct behaviour of the API after future modifications and extensions.

## Content

The `examples` folder contains the following list of exmples for the InterFlex API

* `accesstoken.py`
* `flexibilityActivation.py`
* `flexibilityActivationACK.py`
* `flexibilityActivationNACK.py`
* `flexibilityOffer.py`
* `flexibilityRequest.py`

The sub-folder `request_based_negotiation` contains a set of scripts to perform a full integration test of the flexibility activation scenario decribed in [Deliverable D3.5](https://interflex-h2020.com/wp-content/uploads/2019/11/D3.5-Open-Reference-Implementation_RWTH_InterFlex_v1.0.pdf).



## Contact

[![EONERC ACS Logo](doc/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- Jonas Baude <jonas.baude@eonerc.rwth-aachen.de>
- Amir Ahmadifar <ahmadifar@eonerc.rwth-aachen.de>

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)
[EON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)