import interflexAPI.authentication as auth
import json

def is_json(myjson):
    try:
        json_object = json.loads(myjson)
    except ValueError as e:
        return False
    return True


host = ""
port = ""
username = ""
user_secret = ""
client_id = ""
client_secret = ""

response = auth.authenticate(host, port, username, user_secret, client_id, client_secret)
response = json.loads(response)

if 'access_token' in response:
    access_token = response["access_token"]
    refresh_token = response["refresh_token"]

else:
    print("error!")
    print(response)
    exit()

print("Response to access_token request: " + json.dumps(response))

tokens = auth.refresh(host,port, refresh_token, client_id, client_secret)

print("Response to access_token refresh request: " + json.dumps(response))
