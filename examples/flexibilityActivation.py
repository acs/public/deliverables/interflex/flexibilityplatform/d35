import datetime
import interflexAPI.authentication as auth
import interflexAPI.flexibilityActivation as fa 
import json

host = ""
port = ""
username = ""
user_secret = ""
client_id = ""
client_secret = ""

response = auth.authenticate(host, port, username, user_secret, client_id, client_secret)
response = json.loads(response)
if 'access_token' in response:
    access_token = response["access_token"]
    refresh_token = response["refresh_token"]
else:
    print("error!")
    print(response)
    exit()

activation_id = "a1234"
offer_id = "o1234"
entity_id = "dso1"
aggregator = "agg1"

timestamp = datetime.datetime.utcnow().isoformat()

flex_activation = fa.create_activation(activation_id, timestamp, offer_id, 0, 0, 0, 0, 0, 0, 0, 0)
print(json.dumps(flex_activation))

r = fa.post_activation(host, port, entity_id, aggregator, access_token, flex_activation)
print(r)

r = fa.get_all_activations(host, port, entity_id, access_token, aggregator)
print(r)

r = fa.delete_activation(host, port, entity_id, access_token, aggregator, activation_id)
print(r)
