import datetime
import interflexAPI.authentication as auth
import interflexAPI.flexibilityActivationACK as faACK 
import json

host = ""
port = ""
username = ""
user_secret = ""
client_id = ""
client_secret = ""

response = auth.authenticate(host, port, username, user_secret, client_id, client_secret)
response = json.loads(response)
if 'access_token' in response:
    access_token = response["access_token"]
    refresh_token = response["refresh_token"]
else:
    print("error!")
    print(response)
    exit()

activation_ack_id = "a123456"
offer_id = "o1234"
entity_id = "agg1"
aggregator = "agg1"

timestamp = datetime.datetime.utcnow().isoformat()

flex_ack = faACK.create_flex_activation_ack(activation_ack_id, timestamp, offer_id, 0, 0, 0, 0, 0)
print(json.dumps(flex_ack))

r = faACK.post_activation_ack(host, port, aggregator, access_token, flex_ack)
print(r)

r = faACK.get_all_activation_acks(host, port, access_token, aggregator)
print(r)

r = faACK.delete_activation_ack(host, port, access_token, aggregator, activation_ack_id)
print(r)
