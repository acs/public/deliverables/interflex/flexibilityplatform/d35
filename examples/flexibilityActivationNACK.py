import datetime
import interflexAPI.authentication as auth 
import interflexAPI.flexibilityActivationNACK as faNACK 
import json

host = ""
port = ""
username = "admin@test.com"
user_secret = "1234"
client_id = "0b5fd733-16f2-4f12-8420-d6bd3c5f79f5"
client_secret = "f5862e6c-5dfe-4277-ab3d-93ec2a85f432"

response = auth.authenticate(host, port, username, user_secret, client_id, client_secret)
response = json.loads(response)
if 'access_token' in response:
    access_token = response["access_token"]
    refresh_token = response["refresh_token"]
else:
    print("error!")
    print(response)
    exit()

activation_nack_id = "a123456"
offer_id = "o1234"
entity_id = "agg1"
aggregator = "agg1"

timestamp = datetime.datetime.utcnow().isoformat()

flex_nack = faNACK.create_flex_activation_nack(activation_nack_id, timestamp, offer_id, 0, 0, 0, 0, 0)
print(json.dumps(flex_nack))

r = faNACK.post_activation_nack(host, port, aggregator, access_token, flex_nack)
print(r)

r = faNACK.get_all_activation_nacks(host, port, access_token, aggregator)
print(r)

r = faNACK.delete_activation_nack(host, port, access_token, aggregator, activation_nack_id)
print(r)
