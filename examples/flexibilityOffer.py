import datetime
import interflexAPI.flexibilityOffer as fo
import interflexAPI.authentication as auth
import json

host = "localhost"
port = "3000"
username = "agg1@agg1.com"
user_secret = "agg1"
client_id = "0b5fd733-16f2-4f12-8420-d6bd3c5f79f5"
client_secret = "f5862e6c-5dfe-4277-ab3d-93ec2a85f432"

response = auth.authenticate(host, port, username, user_secret, client_id, client_secret)
response = json.loads(response)
if 'access_token' in response:
    access_token = response["access_token"]
    refresh_token = response["refresh_token"]
else:
    print("error!")
    print(response)
    exit()

response_time = 15 #minutes
offer_id = "s1234"
entity_id = "agg1"

timestamp = datetime.datetime.utcnow().isoformat()
deadline = (datetime.datetime.utcnow()+datetime.timedelta(minutes=response_time)).isoformat()

offer = fo.create_offer(offer_id, entity_id, timestamp, deadline, "0815",452, 0, 0, 0, 0, 0, 0, 0)
print(json.dumps(offer))

r = fo.post_offer(host, port, entity_id, access_token, offer)
print(r)

r = fo.get_all_offers(host, port, access_token)
print(r) 

r = fo.delete_offer(host, port, entity_id, access_token, offer_id)
print(r)
