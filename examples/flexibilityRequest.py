import datetime
import interflexAPI.authentication as auth
import interflexAPI.flexibilityRequest as fr 
import json

host = "localhost"
port = "3000"
username = "admin@test.com"
user_secret = "1234"
client_id = "0b5fd733-16f2-4f12-8420-d6bd3c5f79f5"
client_secret = "f5862e6c-5dfe-4277-ab3d-93ec2a85f432"

response = auth.authenticate(host, port, username, user_secret, client_id, client_secret)
response = json.loads(response)
if 'access_token' in response:
    access_token = response["access_token"]
    refresh_token = response["refresh_token"]
else:
    print("error!")
    print(response)
    exit()

response_time = 15 #minutes
request_id = "s123456789"
entity_id = "dso1"

timestamp = datetime.datetime.utcnow().isoformat()
deadline = (datetime.datetime.utcnow()+datetime.timedelta(minutes=response_time)).isoformat()

flex_request = fr.create_request(request_id, timestamp, deadline, "0816", 0, 0, 0, 0)
print(json.dumps(flex_request))

r = fr.post_request(host, port, entity_id, access_token, flex_request)
print(r)

r = fr.get_all_requests(host, port, entity_id, access_token)
requestList = json.loads(r)
print(json.dumps(requestList,indent=4))
#print(r)

#r = fr.delete_request(host, port, entity_id, access_token, request_id)
#print(r)
