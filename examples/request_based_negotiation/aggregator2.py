#!/usr/bin/python3.6
import datetime
import json
import time
import random
import interflexAPI.authentication as auth
import interflexAPI.flexibilityActivation as fa
import interflexAPI.flexibilityOffer as fo
import interflexAPI.flexibilityRequest as fr
import interflexAPI.flexibilityActivationACK as faACK
import interflexAPI.flexibilityActivationNACK as faNACK
from operator import attrgetter

# dummy: create a random pice for the offer
def get_flex_price():
    price = random.uniform(0.5, 2.5)
    price = round(price, 2)
    return price

# create an offer corresponding to the requested flexibility
def provide_offer(request):
    global host, port, entity_id, access_token, outstanding_offers
    response_time = 5

    # create offerID and timestamps
    offer_id = fo.get_offer_id(host, port, entity_id, access_token)
    timestamp = datetime.datetime.utcnow().isoformat()
    deadline = (datetime.datetime.utcnow()+datetime.timedelta(minutes=response_time)).isoformat()

    # evaluate flex and provide available flex; here the requested amount
    flexible_power = request["flexible_power"]

    # create offer object
    offer = fo.create_offer(offer_id, entity_id, timestamp, deadline, my_region_id, flexible_power, -1, -1, -1, -1, get_flex_price(), 0, "EUR")
    #print(offer)

    # post offer to platform
    r = fo.post_offer(host, port, entity_id, access_token, offer)
    #print(r)
    
    print("providing the following offer:")
    print(json.dumps(offer,indent=4))

    # save offer Id
    outstanding_offers.append(offer_id)

def handleActivation(act):
    global host, port, access_token, entity_id, outstanding_offers
    print("received the following activation request")
    print(json.dumps(act,indent=4))
    print("going to activate flexibility...")
    time.sleep(1)
    ack = faACK.create_flex_activation_ack(0,0,0,0,0,0,0,0)
    #faACK.post_activation_ack(host, port, entity_id, activation_delay, ack)
    print("Flexibility activated!")
    outstanding_offers.remove(act['offer_id'])
    

####  main #####
host = "localhost"
port = "3000"
client_id = "0b5fd733-16f2-4f12-8420-d6bd3c5f79f5"
client_secret = "f5862e6c-5dfe-4277-ab3d-93ec2a85f432"
username = "admin@test.com"
user_secret = "1234"
entity_id = "agg2"
my_region_id = "081717"
target_dso = "dso1"
response = auth.authenticate(host, port, username, user_secret, client_id, client_secret)
response = json.loads(response)

if 'access_token' in response:
    access_token = response["access_token"]
    refresh_token = response["refresh_token"]

else:
    print("error!")
    print(response)
    exit()

# init for demo:
outstanding_requests= []
outstanding_offers= []
requestList = fr.get_all_requests(host, port, target_dso, access_token)
requestList = json.loads(requestList)
for req in requestList:
    outstanding_requests.append(req["id"])
print("initialized!")


# main loop polling for requests
while 1:
    #print("checking for new requests...")
    
    # poll requests
    requestList = fr.get_all_requests(host, port, target_dso, access_token)
    requestList = json.loads(requestList)

    # check relevant region ID
    for req in requestList:
        if req['region_id'] == my_region_id and req['id'] not in outstanding_requests:
            print("recieved a flexibilty request:")
            #print(req['id'])
            print(json.dumps(req,indent=4))
            #print(req)
            provide_offer(req)
            outstanding_requests.append(req["id"])
            
    # poll activation
    activationList = fa.get_all_activations(host, port, entity_id, access_token, entity_id)
    activationList = json.loads(activationList)

    #print(outstanding_offers)

    # check for new activations
    for act in activationList:
        if act['offer_id'] in outstanding_offers:
            handleActivation(act)

    #print(outstanding_offers)
    #print(requestList)

    #print(json.dumps(requestList,indent=4))
    #relevant_requests = requestList.index(min(offerList, key=lambda x: x["flex_price"]))

    time.sleep(2)

exit()
