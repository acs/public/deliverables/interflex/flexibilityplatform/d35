#!/usr/bin/python3.6
import datetime
import interflexAPI.authentication as auth
import interflexAPI.flexibilityActivation as fa
import interflexAPI.flexibilityOffer as fo
import interflexAPI.flexibilityRequest as fr
import interflexAPI.flexibilityActivationACK as faACK
import interflexAPI.flexibilityActivationNACK as faNACK

import json
from operator import attrgetter
import time

host = "localhost"
port = "3000"
client_id = "0b5fd733-16f2-4f12-8420-d6bd3c5f79f5"
client_secret = "f5862e6c-5dfe-4277-ab3d-93ec2a85f432"
username = "admin@test.com"
user_secret = "1234"
entity_id = "dso1"
response_time = 0.5 # minutes
my_region_id = "081717"


response = auth.authenticate(host, port, username, user_secret, client_id, client_secret)
response = json.loads(response)

if 'access_token' in response:
    access_token = response["access_token"]
    refresh_token = response["refresh_token"]

else:
    print("error!")
    print(response)
    exit()

timestamp = datetime.datetime.utcnow().isoformat()
deadline = (datetime.datetime.utcnow()+datetime.timedelta(minutes=response_time)).isoformat()

# get a new RequestID from the Platform
request_id = fr.get_request_id(host, port, entity_id, access_token)
#print(request_id)
#exit()

# Post FlexibilityRequest to Platform
flex_request = fr.create_request(request_id, timestamp, deadline, my_region_id, 5,0,20,"KWh")
print(json.dumps(flex_request, indent=4))
r = fr.post_request(host, port, entity_id, access_token, flex_request)
print(r)
print("ok")

# Wait
time.sleep(response_time * 60)

# Get FlexibilityOffers from Platform
offerList= []
tmpOfferList = fo.get_all_offers(host, port, access_token)
tmpOfferList = json.loads(tmpOfferList)
#offerList = [x for x in offerList if not x["region_id"] == my_region_id]
for obj in tmpOfferList: 
    if (obj['region_id'] == my_region_id):
        offerList.append(obj)
print(json.dumps(offerList,indent=4))


# Simplest selection: select cheapest offer
selected_offer = offerList.index(min(offerList, key=lambda x: x["flex_price"]))
print(selected_offer)

# Post FlexibilityActivation to Platform
activation_id = fa.get_activation_id(host, port, entity_id, access_token)
timestamp = datetime.datetime.utcnow().isoformat()
offer_id = offerList[selected_offer]["id"]
flexible_power = offerList[selected_offer]["flexible_power"]
duration = "20"
activation_delay = 120 #minutes
aggregator = offerList[selected_offer]["entity_id"]
price = offerList[selected_offer]["flex_price"]
activation_time = (datetime.datetime.utcnow()+datetime.timedelta(minutes=activation_delay)).isoformat()
activation_request = fa.create_activation(activation_id, timestamp, offer_id, flexible_power,0, 20, "KWh", activation_time, price ,0,"EUR")
print(json.dumps(activation_request, indent=4))
fa.post_activation(host, port, entity_id, aggregator, access_token, activation_request)

# wait for response
acks = faACK.get_all_activation_acks
nacks = faNACK.get_all_activation_nacks

# Delete FlexibilityRequest
r = fr.delete_request(host, port, entity_id, access_token, request_id)
print(r)
